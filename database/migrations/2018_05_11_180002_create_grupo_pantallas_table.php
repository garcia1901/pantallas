<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrupoPantallasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_pantallas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('contenido_id')->unsigned();
            $table->foreign('contenido_id')->references('id')->on('contenidos')->onDelete('cascade');

            //$table->unsignedInteger('hentai_id');
            $table->integer('pantalla_id')->unsigned();
            $table->foreign('pantalla_id')->references('id')->on('pantallas')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_pantallas');
    }
}
