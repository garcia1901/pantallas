<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archivos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('contenido_id')->unsigned();
            $table->foreign('contenido_id')->references('id')->on('contenidos')->onDelete('cascade');

            //$table->unsignedInteger('hentai_id');
            $table->integer('pantalla_id')->unsigned();
            $table->foreign('pantalla_id')->references('id')->on('pantallas')->onDelete('cascade');


            $table->dateTime('inicio')->nullable();
            $table->dateTime('final')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archivos');
    }
}
