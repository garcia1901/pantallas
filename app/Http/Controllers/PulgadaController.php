<?php

namespace App\Http\Controllers;

use App\Pulgada;
use Illuminate\Http\Request;

class PulgadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pulgadas.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pulgadas.create', compact(''));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pulgada  $pulgada
     * @return \Illuminate\Http\Response
     */
    public function show(Pulgada $pulgada)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pulgada  $pulgada
     * @return \Illuminate\Http\Response
     */
    public function edit(Pulgada $pulgada)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pulgada  $pulgada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pulgada $pulgada)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pulgada  $pulgada
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pulgada $pulgada)
    {
        //
    }
}
