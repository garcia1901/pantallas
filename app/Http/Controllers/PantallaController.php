<?php

namespace App\Http\Controllers;

use App\Pantalla;
use Illuminate\Http\Request;
use Mockery\Generator\Parameter;

class PantallaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$data = Pantalla::all();
        return view('admin.pantallas.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pantallas.create', compact(''));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pantalla  $pantalla
     * @return \Illuminate\Http\Response
     */
    public function show(Pantalla $pantalla)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pantalla  $pantalla
     * @return \Illuminate\Http\Response
     */
    public function edit(Pantalla $pantalla)
    {

        //return view('admin.pantallas.create', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pantalla  $pantalla
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pantalla $pantalla)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pantalla  $pantalla
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pantalla $pantalla)
    {
        //
    }
    public function listahoras()
    {
        return view('admin.pantallas.horas', compact(''));

    }

}
