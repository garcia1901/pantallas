@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Log de Actividad
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap4.css') }}"/>

<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css"/>

@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Log de Actividad</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>

            <li class=" breadcrumb-item active">Log de Actividad</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12 col-sm-12">
            <div class="card panel-primary ">
                <div class="card-heading">
                    <h4 class="card-title">
                        Log de Activdad
                    </h4>
                </div>
                <div class="card-body">
                    <table id="table" class=" table table-bordered  dataTable no-footer table-responsive-lg table-responsive-sm table-responsive-md" cellspacing="0" width="100%">
                        <thead>
                        <tr class="filters">
                            <th>Dni</th>
                            <th>Email</th>
                            <th>Usuario</th>
                            <th>Descripción</th>
                            <th>Fecha de Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>12345678</th>
                            <th>User1@mail.com</th>
                            <th>user1</th>
                            <th>Agrego X contenido</th>
                            <th>2018-05-11 15:36:39</th>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div><!-- row-->


    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap4.js') }}"></script>
<script>
$(function() {

    $('#table').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
        },
        "order": [[ 1, "desc" ]]
    });


    /*var table = $('#table').DataTable({
        "order": [[3, "desc"]],
        processing: true,
        serverSide: true,
        //ajax: "{!! route('admin.activity_log.data') !!}",
        columns: [
            { data: 'causer_id', name: 'causer_id' },
            { data: 'log_name', name: 'log_name' },
            { data: 'description', name: 'description' },
            { data: 'created_at', name:'created_at'}
        ]
    });*/
});
</script>
@stop
