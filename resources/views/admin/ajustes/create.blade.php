@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Ajustes
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
    <style>

        .container{
            margin-top:20px;
        }
        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
        .image_radius{
            border-top-right-radius: 4px !important;
            border-top-left-radius: 0 !important;
            border-bottom-left-radius: 0 !important;
            border-bottom-right-radius: 4px !important;
        }
        .fileinput .thumbnail > img{
            width:100%;
        }
        .color_a{
            color: #333;
        }
        .btn-file > input{
            width: auto;
        }
    </style>

@stop
{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Ajustes</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">Ajustes</a>
            </li>
            <li class="active">Configurar Ajustes</li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <!--main content-->
        <div class="row">
            <!--row starts-->
            <div class="col-md-12 col-lg-6 col-sm-6 col-12">
                <!--lg-6 starts-->
                <!--basic form starts-->
                <div class="my-3">
                    <div class="card panel-primary" id="hidepanel1">
                        <div class="card-heading">
                            <h3 class="card-title">

                                Actualizar Ajustes
                            </h3>
                            <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-remove removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" action="#">
                                <!-- CSRF Token -->

                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 col-lg-3 col-12 control-label" for="marca">Titulo</label>
                                        <div class="col-md-9 col-lg-9 col-12">
                                            <input name="nombre" type="text" placeholder="Nombre" class="form-control"></div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 col-lg-3 col-12 control-label" for="marca">Url del Sitio</label>
                                        <div class="col-md-9 col-lg-9 col-12">
                                            <input name="url" type="text" placeholder="Url" class="form-control"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 col-lg-3 col-12 control-label" for="upload">Favicon</label>
                                        <div class="col-md-9 col-12 col-lg-9">
                                            <div class="input-group image-preview">
                                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none; border-radius:0 !important; border: 1px solid rgba(0, 0, 0, 0.16);">
                        <span class="fa  fa-remove"></span> Clear
                    </button>
                                                    <!-- image-preview-input -->
                    <div class="btn btn-default image_radius image-preview-input" style="margin-left:-3px;">
                        <span class="fa fa-folder-open"></span>
                        <span class="image-preview-input-title">Buscar</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                    </div>
                </span>
                                            </div><!-- /input-group image-preview [TO HERE]-->
                                        </div>

                                    </div>

                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 col-lg-3 col-12 control-label" for="upload">Logo</label>
                                        <div class="col-md-9 col-12 col-lg-9">
                                            <div class="input-group image-preview">
                                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none; border-radius:0 !important; border: 1px solid rgba(0, 0, 0, 0.16);">
                        <span class="fa  fa-remove"></span> Clear
                    </button>
                                                    <!-- image-preview-input -->
                    <div class="btn btn-default image_radius image-preview-input" style="margin-left:-3px;">
                        <span class="fa fa-folder-open"></span>
                        <span class="image-preview-input-title">Buscar</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                    </div>
                </span>
                                            </div><!-- /input-group image-preview [TO HERE]-->
                                        </div>

                                    </div>

                                </div>



                                <!-- Message body -->
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 col-lg-3 col-12 control-label" for="message">Descripción</label>
                                        <div class="col-md-9 col-lg-9 col-12">
                                            <textarea class="form-control resize_vertical" id="message" name="message" placeholder="" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form actions -->
                                <div class="form-position">
                                    <div class="row">
                                        <div class="col-md-12  col-sm-12 col-12  col-lg-12 text-right">
                                            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--basic form 2 starts-->

            </div>
            <!--md-6 ends-->

        </div>
        <!--md-6 ends-->
        <!--main content ends--> </section>
    <!-- content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });


            $(document).on('click', '#close-preview', function(){
            $('.image-preview').popover('hide');
            // Hover befor close the preview
            $('.image-preview').hover(
                function () {
                    $('.image-preview').popover('show');
                },
                function () {
                    $('.image-preview').popover('hide');
                }
            );
        });

        $(function() {
            // Create the close button
            var closebtn = $('<button/>', {
                type:"button",
                text: 'x',
                id: 'close-preview',
                style: 'font-size: initial;',
            });
            closebtn.attr("class","close pull-right");
            // Set the popover default content
            $('.image-preview').popover({
                trigger:'manual',
                html:true,
                title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
                content: "There's no image",
                placement:'bottom'
            });
            // Clear event
            $('.image-preview-clear').click(function(){
                $('.image-preview').attr("data-content","").popover('hide');
                $('.image-preview-filename').val("");
                $('.image-preview-clear').hide();
                $('.image-preview-input input:file').val("");
                $(".image-preview-input-title").text("Browse");
            });
            // Create the preview image
            $(".image-preview-input input:file").change(function (){
                var img = $('<img/>', {
                    id: 'dynamic',
                    width:250,
                    height:200
                });
                var file = this.files[0];
                var reader = new FileReader();
                // Set preview image into the popover data-content
                reader.onload = function (e) {
                    $(".image-preview-input-title").text("Change");
                    $(".image-preview-clear").show();
                    $(".image-preview-filename").val(file.name);
                    img.attr('src', e.target.result);
                    $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
                reader.readAsDataURL(file);
            });
        });


    </script>

@stop
