<ul id="menu" class="page-sidebar-menu">

    <li {!! (Request::is('admin') ? 'class="active"' : '') !!}>
        <a href="{{ route('admin.dashboard') }}">
            <i class="livicon" data-name="dashboard" data-size="18" data-c="#fff" data-hc="#fff"
               data-loop="true"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>
    <li {!! (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="user" data-size="18" data-c="#ffffff" data-hc="#ffffff"
               data-loop="true"></i>
            <span class="title">Usuarios</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Usuarios
                </a>
            </li>
            <li {!! (Request::is('admin/users/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Agregar Usuario
                </a>
            </li>
            <li {!! ((Request::is('admin/users/*')) && !(Request::is('admin/users/create')) || Request::is('admin/user_profile') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::route('admin.users.show',Sentinel::getUser()->id) }}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver Perfil
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Eliminar Usuarios
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/contenido') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/contenido') }}">
            <i class="livicon" data-name="pen" data-size="18" data-c="#ffffff" data-hc="#ffffff"
               data-loop="true"></i>
            Contenidos
        </a>
    </li>

    <li {!! (Request::is('admin/sede') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/sede') }}">
            <i class="livicon" data-name="flag" data-size="18" data-c="#ffffff" data-hc="#ffffff"
               data-loop="true"></i>
            Sedes
        </a>
    </li>

    <li {!! (Request::is('admin/marca') || Request::is('admin/pulgada') || Request::is('admin/pantalla') || Request::is('admin/grupo-pantalla') || Request::is('admin/modelo') ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/pantalla') }}">
            <i class="livicon" data-name="image" data-c="#ffffff" data-hc="#ffffff" data-size="18"
               data-loop="true"></i>
            <span class="title">Pantallas</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/pantalla') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/pantalla') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Pantallas
                </a>
            </li>
            <li {!! (Request::is('admin/grupo-pantalla') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/grupo-pantalla') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Grupo Pantallas
                </a>
            </li>
            <li {!! (Request::is('admin/marca') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/marca') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Marcas
                </a>
            </li>
            <li {!! (Request::is('admin/modelo') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/modelo') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Modelo
                </a>
            </li>
            <li {!! (Request::is('admin/pulgada') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/pulgada') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Tamaño
                </a>
            </li>

        </ul>
            </li>
    <li {!! (Request::is('admin/activity_log') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/activity_log') }}">
            <i class="livicon" data-name="eye-open" data-size="18" data-c="#ffffff" data-hc="#ffffff"
               data-loop="true"></i>
            Log de Actividad
        </a>
    </li>
    <li {!! (Request::is('admin/ajuste') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/ajuste') }}">
            <i class="livicon" data-name="wrench" data-size="18" data-c="#ffffff" data-hc="#ffffff"
               data-loop="true"></i>
            Ajustes
        </a>
    </li>
    <li {!! (Request::is('admin/logout') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/logout') }}">
            <i class="livicon" data-name="sign-out" data-size="18" data-c="#ffffff" data-hc="#ffffff"
               data-loop="true"></i>
            Cerrar Sesión
        </a>
    </li>
    <!-- Menus generated by CRUD generator -->
    @include('admin/layouts/menu')
</ul>