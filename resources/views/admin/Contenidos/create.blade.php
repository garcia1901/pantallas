@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Form examples
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/dropzone/css/dropzone.css') }}" rel="stylesheet" type="text/css" />

    <style>

        .container{
            margin-top:20px;
        }
        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
        .image_radius{
            border-top-right-radius: 4px !important;
            border-top-left-radius: 0 !important;
            border-bottom-left-radius: 0 !important;
            border-bottom-right-radius: 4px !important;
        }
        .fileinput .thumbnail > img{
            width:100%;
        }
        .color_a{
            color: #333;
        }
        .btn-file > input{
            width: auto;
        }
        .dropzone .dz-preview .dz-image img {
            width :100%;
        }
    </style>

@stop
{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Contenidos</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">Contenidos</a>
            </li>
            <li class="active">Crear Contenido</li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <!--main content-->
        <div class="row">
            <!--row starts-->
            <div class="col-md-12 col-lg-12 col-sm-12 col-12">
                <!--lg-6 starts-->
                <!--basic form starts-->
                <div class="my-3">
                    <div class="card panel-primary" id="hidepanel1">
                        <div class="card-heading">
                            <h3 class="card-title">
                                <i class="livicon" data-name="clock" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                Agregar Contenido
                            </h3>
                            <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-remove removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" action="#">
                                <!-- CSRF Token -->
                                <!-- Name input-->
                                <div class="row">
                                    <div class="col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-3 col-lg-3 col-12 control-label" for="name">Titulo</label>
                                                <div class="col-md-9 col-lg-9 col-12">
                                                    <input id="titulo" name="titulo" type="text" placeholder="Titulo" class="form-control"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-3 col-lg-3 col-12 control-label" for="name">Grupo de Pantallas</label>
                                                <div class="col-md-9 col-lg-9 col-12">
                                                    <select class="form-control" name="pantalla">
                                                        <option value="0"></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-3 col-lg-3 col-12 control-label" for="upload">Imagen</label>
                                                <div class="col-md-9 col-12 col-lg-9">
                                                    <div class="input-group image-preview">
                                                        <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                                        <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none; border-radius:0 !important; border: 1px solid rgba(0, 0, 0, 0.16);">
                        <span class="fa  fa-remove"></span> Clear
                    </button>
                                                            <!-- image-preview-input -->
                    <div class="btn btn-default image_radius image-preview-input" style="margin-left:-3px;">
                        <span class="fa fa-folder-open"></span>
                        <span class="image-preview-input-title">Buscar</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                    </div>
                </span>
                                                    </div><!-- /input-group image-preview [TO HERE]-->
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                <div class="col-md-6 col-lg-6">

                                <!-- Message body -->
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 col-lg-3 col-12 control-label" for="message">Descripción</label>
                                        <div class="col-md-9 col-lg-9 col-12">
                                            <textarea class="form-control resize_vertical" id="message" name="message" placeholder="" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form actions -->
                                <div class="form-position">
                                    <div class="row">
                                        <div class="col-md-12  col-sm-12 col-12  col-lg-12 text-right">
                                            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--basic form 2 starts-->

            </div>
            <!--md-6 ends-->
            <div class="col-md-12 col-lg-12 col-12 my-3">
            <div class="card panel-success" id="hidepanel6">
                <div class="card-heading">
                    <h3 class="card-title">
                        <i class="livicon" data-name="share" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Agregar Multimedia
                    </h3>
                    <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-remove removepanel clickable"></i>
                                </span>
                </div>
                <div class="card-body">
                    <div class="col-md-12" style="padding:30px;">

                        <div class="form-group">
                            <label>Opciones de Archivos:  </label>
                            <label class="radio-inline" style="margin-left: 20px">
                                <input type="radio"  name="optionsRadiosInline" id="option2" value="option2" checked="true">&nbsp;Videos</label>
                            <label class="radio-inline" style="margin-left: 20px">
                                &nbsp;<input type="radio" name="optionsRadiosInline" id="option1" value="option1" >&nbsp; Url Streaming</label>
                            <label class="radio-inline" style="margin-left: 20px">
                                <input type="radio"   name="optionsRadiosInline" id="option3" value="option3">&nbsp; Imagenes</label>
                        </div>
<div id="div1" style="display:block;">
    {!! Form::open(array('url' => URL::to('admin/file/subir'), 'method' => 'post', 'id'=>'my-dropzone','class' => 'dropzone', 'files'=> true)) !!}
    <div class="dz-message">
        Sube tus archivos aquí
    </div>
    <div class="dropzone-previews"></div>
    <button type="submit" class="btn btn-success" id="submit">Guardar</button>
    {!! Form::close() !!}
</div>
                    </div>

                    <form role="form" id="form_controls">
                        <div id="div2" class="form-group" style="display: none;">
                            <div class="row">
                                <label class="col-md-2 col-lg-2 col-12 control-label" for="name">Url del Stream</label>
                                <div class="col-md-10 col-lg-10 col-12">
                                    <input id="titulo" name="url" type="text" placeholder="url" class="form-control"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-2 col-lg-2 col-12 control-label" for="name">Loop</label>
                                <div class="col-md-10 col-lg-10 col-12">
                                    <input name="loop" type="number" placeholder="Veces de Repetición" class="form-control"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Hora Inicio:</label>
                            <div class="input-group">
                                <div class="input-group-append">
                                 <span class="input-group-text">  <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                           </span> </div>
                                <input type="text" class="form-control" data-mask="99/99/9999 99:99:99" placeholder="MM/DD/YYYY HH:mm:ss">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>Hora Final:</label>
                            <div class="input-group">
                                <div class="input-group-append">
                                 <span class="input-group-text">  <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                           </span> </div>
                                <input type="text" class="form-control" data-mask="99/99/9999 99:99:99" placeholder="MM/DD/YYYY HH:mm:ss">
                            </div>
                            <!-- /.input group -->
                        </div>

                        <div class="form-group">
                            <label for="area">Descripción</label>
                            <textarea class="form-control resize_vertical" id="area" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 col-lg-3 col-12 control-label" for="upload">Portada</label>
                                <div class="col-md-9 col-12 col-lg-9">
                                    <div class="input-group image-preview">
                                        <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none; border-radius:0 !important; border: 1px solid rgba(0, 0, 0, 0.16);">
                        <span class="fa  fa-remove"></span> Clear
                    </button>
                                            <!-- image-preview-input -->
                    <div class="btn btn-default image_radius image-preview-input" style="margin-left:-3px;">
                        <span class="fa fa-folder-open"></span>
                        <span class="image-preview-input-title">Buscar</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                    </div>
                </span>
                                    </div><!-- /input-group image-preview [TO HERE]-->
                                </div>

                            </div>
                        <button type="submit" class="btn btn-responsive btn-default">Guardar</button>
                        </div>
                    </form>

                </div>
            </div>
            </div>
        </div>
        <!--md-6 ends-->

        <!--main content ends--> </section>

    <!-- content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form_examples.js') }}"></script>

    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/dropzone/js/dropzone.js') }}" ></script>
    <script>
        $(document).ready(function() {
            $("input[type=radio]").click(function(event){
                var valor = $(event.target).val();
                if(valor =="option2"){
                    $("#div1").show();
                    $("#div2").hide();
                } else if (valor == "option1") {
                    $("#div1").hide();
                    $("#div2").show();
                } else if (valor == "option3") {
                    $("#div1").show();
                    $("#div2").hide();
                }
            });
        });
        $(document).on('click', '#close-preview', function(){
            $('.image-preview').popover('hide');
            // Hover befor close the preview
            $('.image-preview').hover(
                function () {
                    $('.image-preview').popover('show');
                },
                function () {
                    $('.image-preview').popover('hide');
                }
            );
        });

        $(function() {
            // Create the close button
            var closebtn = $('<button/>', {
                type:"button",
                text: 'x',
                id: 'close-preview',
                style: 'font-size: initial;',
            });
            closebtn.attr("class","close pull-right");
            // Set the popover default content
            $('.image-preview').popover({
                trigger:'manual',
                html:true,
                title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
                content: "There's no image",
                placement:'bottom'
            });
            // Clear event
            $('.image-preview-clear').click(function(){
                $('.image-preview').attr("data-content","").popover('hide');
                $('.image-preview-filename').val("");
                $('.image-preview-clear').hide();
                $('.image-preview-input input:file').val("");
                $(".image-preview-input-title").text("Browse");
            });
            // Create the preview image
            $(".image-preview-input input:file").change(function (){
                var img = $('<img/>', {
                    id: 'dynamic',
                    width:250,
                    height:200
                });
                var file = this.files[0];
                var reader = new FileReader();
                // Set preview image into the popover data-content
                reader.onload = function (e) {
                    $(".image-preview-input-title").text("Change");
                    $(".image-preview-clear").show();
                    $(".image-preview-filename").val(file.name);
                    img.attr('src', e.target.result);
                    $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
                reader.readAsDataURL(file);
            });
        });

        //files
        Dropzone.options.myDropzone = {
            autoProcessQueue: false,
            uploadMultiple: true,
            maxFilezise: 100,
            maxFiles: 20,

            init: function() {
                var submitBtn = document.querySelector("#submit");
                myDropzone = this;

                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {
                    //alert("file uploaded");
                    myDropzone.processQueue();
                });

                this.on("complete", function(file) {
                    myDropzone.removeFile(file);
                });

                this.on("success",
                    myDropzone.processQueue.bind(myDropzone)
                );
            }
        };
    </script>

@stop
