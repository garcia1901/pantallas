@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Josh Admin Template
@parent
@stop

{{-- page level styles --}}
@section('header_styles')


<link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
<meta name="_token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/morrisjs/morris.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/dashboard2.css') }}"/>


<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/gmaps/css/examples.css') }}"/>
<link href="{{ asset('assets/css/pages/googlemaps_custom.css') }}" rel="stylesheet">

<style>
   .list_of_items{
        overflow: auto;
        height:20px;
    }
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Cantidad'],
            ['Videos',     11],
            ['Imagenes',      2],
            ['Stream',    7]
        ]);

        var options = {
            title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
</script>
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Bienvenido al Panel de Control   <span class="d-none d-md-inline-block header_info">( Dynamic Dashboard )</span></h1>
    <ol class="breadcrumb">
        <li class=" breadcrumb-item active">
            <a href="#">
                <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
                Dashboard
            </a>
        </li>
    </ol>
</section>

<!--</section>-->
<section class="content">

    <div class="row">
        <div class="col-lg-6 col-xl-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
            <!-- Trans label pie charts strats here-->
            <div class="widget-1">
                <div class="card-body squarebox square_boxs">
                    <div class="col-12 float-left nopadmar">
                        <div class="row">
                            <div class="square_box col-6 text-right">
                                <span>Pantallas</span>

                                <div class="number" id="myTargetElement3"></div>
                            </div>
                            <div class="col-6">
                            <span class="widget_circle3 float-right">
                            <i class="livicon livicon-evo-holder " data-name="eye-open" data-l="true" data-c="#01BC8C"
                                 data-hc="#01BC8C" data-s="40"></i>
                                </span>

                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-3  col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
            <!-- Trans label pie charts strats here-->
            <div class="widget-1">
                <div class="card-body squarebox square_boxs">
                    <div class="col-12 float-left nopadmar">
                        <div class="row">
                            <div class="square_box col-6 text-right">
                                <span>Usuarios</span>

                                <div class="number" id="myTargetElement4"></div>
                            </div>
                            <div class="col-6">
                            <span class="widget_circle4 float-right">
                            <i class="livicon livicon-evo-holder " data-name="user" data-l="true" data-c="#F89A14"
                                data-hc="#F89A14" data-s="40"></i>
                                </span>

                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-3 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
            <!-- Trans label pie charts strats here-->
            <div class="widget-1">
                <div class="card-body squarebox square_boxs">
                    <div class="col-12 float-left nopadmar">
                        <div class="row">
                            <div class="square_box col-6 text-right">
                                <span>Contenidos</span>

                                <div class="number" id="myTargetElement1"></div>
                            </div>
                            <div class="col-6">
                            <span class="widget_circle1 float-right">
                         <i class="livicon livicon-evo-holder " data-name="flag" data-l="true" data-c="#e9573f"
                            data-hc="#e9573f" data-s="40"></i>
                                </span>

                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-3 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
            <!-- Trans label pie charts strats here-->
            <div class="widget-1">
                <div class="card-body squarebox square_boxs">
                    <div class="col-12 float-left nopadmar">
                        <div class="row">
                            <div class="square_box col-6 text-right">
                                <span>Sedes</span>

                                <div class="number" id="myTargetElement2"></div>
                            </div>
                            <div class="col-6">
                            <span class="widget_circle2 float-right">
 <i class="livicon livicon-evo-holder " data-name="pen" data-l="true" data-c="#418BCA"
    data-hc="#418BCA" data-s="40"></i>
                                </span>

                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--/row-->
    <div class="row ">
        <div class="col-md-12 col-sm-12 col-lg-8 col-12 no_padding">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-12">
                    <div class="card panel-border main_chart">
                        <div class="card-heading ">
                            <h3 class="card-title">
                                <i class="livicon" data-name="barchart" data-size="16" data-loop="true" data-c="#EF6F6C"
                                   data-hc="#EF6F6C"></i> Pantallas por Marca
                            </h3>
                        </div>
                        <div class="card-body">
                            <div id="bar-chart" class="flotChart"></div>

                        </div>
                    </div>
                </div>

            </div>
            <br>
            <div class="row">

                <div class="col-md-12 col-lg-12 col-12">
                    <div class="card panel-border map">

                        <div class="card-heading">
                            <h4 class="card-title">Nuestras Sedes</h4>
                            <span class="float-right">
                                    <i class="fa fa-chevron-up showhide clickable"></i>
                                    <i class="fa fa-remove removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="card-body" style="padding:10px !important;">
                            <div id="gmap-markers" class="gmap"></div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

        <div class="col-lg-4 col-md-12 col-sm-12 col-12 my-lg-0 my-3 my-md-3 my-sm-0">
            <div class="card panel-border">
                <div class="card-heading border-light">
                    <h3 class="card-title">
                        <i class="livicon" data-name="users" data-size="18" data-color="#00bc8c" data-hc="#00bc8c"
                           data-l="true"></i>
                        Ültimos Usuarios
                    </h3>
                </div>
                <div class="card-body nopadmar users">
                    @foreach($users as $user )
                    <div class="media">
                        <div>
                            @if($user->pic)
                            <img src="{{ $user->pic }}"
                                 class="media-object rounded-circle">
                            @else
                            <img src="{{ asset('assets/images/authors/no_avatar.jpg') }}"
                                 class="media-object rounded-circle">
                            @endif
                        </div>
                        <div class="media-body ml-3">
                            <h5 class="media-heading">{{ $user->full_name }}</h5>
                            <p>{{ $user->email }}  <span
                                    class="user_create_date float-right">{{ $user->created_at->format('d M') }} </span>
                            </p>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>

            <div class="card panel-border my-3">
                <div class="card-heading">
                    <h4 class="card-title">
                        <i class="livicon" data-name="eye-open" data-size="16" data-loop="true" data-c="#EF6F6C"
                           data-hc="#EF6F6C"></i>
                        Contenido Multimedia
                    </h4>

                </div>
                <div class="card-body nopadmar">
                    <div id="piechart"></div>
                </div>
            </div>

        </div>
    </div>
</section>
<div class="modal fade" id="editConfirmModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Alert</h4>
            </div>
            <div class="modal-body">
                <p>You are already editing a row, you must save or cancel that row before edit/delete a new row</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
<!--for calendar-->
<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
<!-- Back to Top-->
<script type="text/javascript" src="{{ asset('assets/vendors/countUp.js/js/countUp.js') }}"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyADWjiTRjsycXf3Lo0ahdc7dDxcQb475qw&libraries=places"></script>
<script src="{{ asset('assets/vendors/morrisjs/morris.min.js') }}"></script>
<!-- Chart -->

<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/flotchart/js/jquery.flot.js') }}" ></script>
<script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/customcharts.js') }}" ></script>



<script type="text/javascript" src="{{ asset('assets/js/pages/maps_api.js') }}"></script>
<script type="text/javascript"
        src="http://maps.google.com/maps/api/js?key=AIzaSyADWjiTRjsycXf3Lo0ahdc7dDxcQb475qw&libraries=places"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/gmaps/js/gmaps.min.js') }}"></script>

<script>
    var useOnComplete = false,
            useEasing = false,
            useGrouping = false,
            options = {
                useEasing: useEasing, // toggle easing
                useGrouping: useGrouping, // 1,000,000 vs 1000000
                separator: ',', // character to use as a separator
                decimal: '.' // character to use as a decimal
            };
    var demo = new CountUp("myTargetElement1", 12.52, {{ $pageVisits }}, 0, 6, options);
    demo.start();
    var demo = new CountUp("myTargetElement2", 1, {{ $blog_count }}, 0, 6, options);
    demo.start();
    var demo = new CountUp("myTargetElement3", 24.02, {{ $visitors }}, 0, 6, options);
    demo.start();
    var demo = new CountUp("myTargetElement4", 125, {{ $user_count }}, 0, 6, options);
    demo.start();

    $(function () {

        var map4;
        map4 = new GMaps({
            el: '#gmap-markers',
            lat: -12.043333,
            lng: -77.028333,
            zoom: 5,
        });
        map4.addMarker({
            lat: -16.3988900,
            lng: -71.5350000,
            title: 'Arequipa',
            details: {
                database_id: 42,
                author: 'HPNeo'
            },
            click: function (e) {
                if (console.log)
                    console.log(e);
                //alert('You clicked in this marker');
            },
            mouseover: function (e) {
                if (console.log)
                    console.log(e);
            }
        });
        map4.addMarker({
            lat: -13.5226400,
            lng:  -71.9673400,
            title: 'Cuzco',
            details: {
                database_id: 42,
                author: 'HPNeo'
            },
            click: function (e) {
                if (console.log)
                    console.log(e);
                //alert('You clicked in this marker');
            },
            mouseover: function (e) {
                if (console.log)
                    console.log(e);
            }
        });
        map4.addMarker({
            lat: -8.1159900,
            lng: -79.0299800,
            title: 'Trujillo',
            details: {
                database_id: 42,
                author: 'HPNeo'
            },
            click: function (e) {
                if (console.log)
                    console.log(e);
                //alert('You clicked in this marker');
            },
            mouseover: function (e) {
                if (console.log)
                    console.log(e);
            }
        });
        map4.addMarker({
            lat: -12.043333,
            lng: -77.03,
            title: 'Lima',
            details: {
                database_id: 42,
                author: 'HPNeo'
            },
            click: function (e) {
                if (console.log)
                    console.log(e);
                //alert('You clicked in this marker');
            },
            mouseover: function (e) {
                if (console.log)
                    console.log(e);
            }
        });
        map4.addMarker({
            lat: -12.042,
            lng: -77.028333,
            title: 'Marker with InfoWindow',
            infoWindow: {
                content: '<p></p>'
            }
        });



        $('.gmap').closest('.card-body').on('resize', function () {
            $(window).trigger('resize');
        });
    });

</script>
{!! Charts::scripts() !!}
{!! $db_chart->script() !!}
{!! $user_roles->script() !!}

@stop
